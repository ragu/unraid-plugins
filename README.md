# Preclear Plugin for unRAID 6.8.3+ server

Adapted from `gfjardim` [repo](https://github.com/gfjardim/unRAID-plugins) to combine our standalone custom preclear script we have been using for 10 years into the nice GUI.

Our code is NOT as *tight* as it could be, but shell scripting is *extremely* tempermental and OS-dependent. Despite being redundant code, it *finally* works, and any work done towards making it concise usually leads to bugs.

*Done* is better than *Perfect*, and we don't want to waste further time on this.


## Building

Once code is ready to be deployed to unRAID server, follow the following steps to get it to install on unRAID.

1. Run `build.sh` in the top level directory. This will create compressed plugin along with updating plg file version.
2. Commit all files and push to repository.
3. Since the code needs to be deployed via `raw` bitbucket link (CDN delivery system), it can take 5-10 minutes for changes to reflect onto CDNs. Therefore, any deployment from bitbucket onto server immediately after a push *may not* reflect most recent changes, and one should confirm.


## Deploying

1. On the unRAID server, install manually via Plugin Manager using this **[link](https://bitbucket.org/ragu/unraid-plugins/raw/master/plugins/preclear.disk.plg)**. If a version has already been deployed on the day, plugin must be manually removed before redeployment onto the server.


## Integrating Upstream Changes

We have followed the steps from this gist on [forking from Github to Bitbucket](https://gist.github.com/sangeeths/9467061). If recloning this repository, make sure to follow these steps.

1. Switch to our personal `gfjardim` branch, e.g. `git checkout gfjardim`
2. Merge in upstream changes via `git pull sync master`
3. Submit a pull request against master, but do **NOT** delete branch on closure of pull request.
4. Monitor what changes are present, and incorporate as many as possible without breaking our new functionality.
5. Build and push via instructions in `Building` section above.


## References
1. [`dd` invocation (GNU coreutils)](https://www.gnu.org/software/coreutils/manual/html_node/dd-invocation.html#dd-invocation)
2. [Why does a shell variable containing a piped command fail while a non-piped command works as intended?](https://stackoverflow.com/questions/63090704/why-does-a-shell-variable-containing-a-piped-command-fail-while-a-non-piped-comm) - We should use functions instead of strings containing commands, especially for piped commands