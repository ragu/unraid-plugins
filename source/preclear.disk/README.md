**Preclear Disks**

This plugin is a parser for **Joe L.'s** excellent Preclear Disk Script. It also includes a fast post-read verify option, courtesy of **bjp999**. The extended script incorporates the multiple write cycles, each writing different bit patterns to stress the magnetic nature of the drive, as performed in **rvijay007** original `preclear_disk_ext.sh` script.