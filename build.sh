 #!/bin/zsh

# Create path to where plugin will be stored on server
rm -rf usr/
mkdir -p usr/local/emhttp/plugins/
cp -R source/preclear.disk usr/local/emhttp/plugins/

# Archive code into a compressed tar+xz file
# Using a single `tar -zvf` or `zip` does NOT work
date=$(date +%Y.%m.%d)
tar --exclude='.*' -cvf "preclear.disk-$date.tar" usr 2>/dev/null 
xz -z "preclear.disk-$date.tar"
mv "preclear.disk-$date.tar.xz" "preclear.disk-$date.txz"
md5 -q "preclear.disk-$date.txz" > "preclear.disk-$date.md5"
mv "preclear.disk-$date.txz" archive/
mv "preclear.disk-$date.md5" archive/

# This is the ONLY way we found to get sed to work on a Mac.
sed -i '' "s/.*ENTITY version.*/\<!ENTITY version   \"$date\">/" ./plugins/preclear.disk.plg

# Remove temporary usr/ directory
rm -rf usr/